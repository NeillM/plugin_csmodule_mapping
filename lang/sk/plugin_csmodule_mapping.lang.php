<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['restnodata'] = 'No data returned';
$string['filterfields'] = 'Argument sent to the web service to indicate which mapping to do.';
$string['isconnectedquery'] = 'Argument sent to the web service to tell it how to execute the query. Should be set to "N".';
$string['maxrows'] = 'The maximum number of row the web service will return. Should be set to 1.';
$string['password'] = 'The password required by the sms web service.';
$string['prompt_uniquepromptname'] = 'Argument sent to the web service to indicate which mapping to do.';
$string['ssl_verify'] = 'Enable/Disable verifying the ssl connection.';
$string['timeout'] = 'Time limit in seconds before Rogo gives up calling the web service if no response.';
$string['url'] = 'The url of the web service.';
$string['username'] = 'The username required by the web service.';